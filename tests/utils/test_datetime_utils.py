import pytest

from src.utils.datetime_utils import (
    get_scoring_date,
    get_train_period_end,
    get_evaluate_period_date
)


@pytest.mark.parametrize(
    'run_date,scoring_date',
    [
        ('2020-09-01', '2020-10-01'),
        ('2020-09-05', '2020-10-01'),
        ('2020-09-30', '2020-10-01')
    ]
)
def test_get_scoring_date(run_date, scoring_date):
    assert get_scoring_date(run_date) == scoring_date


@pytest.mark.parametrize(
    'scoring_date,train_end',
    [
        ('2020-09-01', '2020-07-31'),
        ('2020-09-05', '2020-07-31'),
        ('2020-10-01', '2020-08-31')
    ]
)
def test_get_train_period_end(scoring_date, train_end):
    assert get_train_period_end(scoring_date, 1) == train_end


@pytest.mark.parametrize(
    'run_date,evaluation_date',
    [
        ('2020-09-01', '2020-08-01'),
        ('2020-09-05', '2020-08-01'),
        ('2020-09-30', '2020-08-01')
    ]
)
def test_get_evaluate_period_date(run_date, evaluation_date):
    assert get_evaluate_period_date(run_date, 1) == evaluation_date
