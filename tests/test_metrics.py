import math
import pytest

from src.evaluate.metrics import precision_at_k_score, recall_at_k_score, lift_score


class TestPrecisionAtKScore:

    @pytest.mark.parametrize('k', list(range(0, 100, 10)))
    def test_empty(self, k):
        assert precision_at_k_score([], [], [], k) == 0

    @pytest.mark.parametrize('k,expected_score', [(3, 1), (6, 0.75)])
    def test_precision_at_k(self, k, expected_score):

        actual = [1, 0, 1, 0, 1, 1, 1]
        predicted = [1, 0, 1, 1, 1, 0, 0]
        predicted_probas = [0.8, 0.2, 0.6, 0.6, 0.9, 0.1, 0.1]

        score = precision_at_k_score(actual, predicted, predicted_probas, k)

        assert isinstance(score, float)
        assert score == expected_score


class TestRecallAtKScore:

    @pytest.mark.parametrize('k', list(range(0, 100, 10)))
    def test_empty(self, k):
        assert recall_at_k_score([], [], [], k) == 0

    @pytest.mark.parametrize('k,expected_score', [(3, 1.0), (6, 0.75)])
    def test_recall_at_k(self, k, expected_score):

        actual = [1, 0, 1, 0, 1, 1, 1]
        predicted = [1, 1, 0, 1, 1, 1, 0]
        predicted_probas = [0.8, 0.7, 0.3, 0.6, 0.9, 0.8, 0.1]

        score = recall_at_k_score(actual, predicted, predicted_probas, k)

        assert isinstance(score, float)
        assert score == expected_score


class TestLift:

    @pytest.mark.parametrize('k', list(range(0, 100, 10)))
    def test_empty(self, k):

        score = lift_score([], [], [], k)
        assert math.isnan(score)

    @pytest.mark.parametrize('k,expected_score', [(3, 1.4), (6, 1.05)])
    def test_recall_at_k(self, k, expected_score):
        actual = [1, 0, 1, 0, 1, 1, 1]
        predicted = [0, 1, 1, 0, 1, 1, 0]
        predicted_probas = [0.2, 0.7, 0.9, 0.3, 0.9, 0.8, 0.1]

        score = lift_score(actual, predicted, predicted_probas, k)

        assert isinstance(score, float)
        assert score == expected_score

    def test_lift_score(self):
        true_labels = [0, 0, 1, 0, 0, 1, 1, 1, 1, 1]
        prediction = [1, 0, 1, 0, 0, 0, 0, 1, 0, 0]

        ls = lift_score(true_labels, prediction, prediction, k=len(true_labels))
        print(ls)