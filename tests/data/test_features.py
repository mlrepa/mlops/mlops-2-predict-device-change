import pytest
import pandas as pd

from src.data.features import add_feature31


@pytest.fixture(scope='module')
def test_df():

    # Generate test data
    test_data = {
        'user_id': [0, 0, 0, 0, 0],
        'month': [
            pd.Timestamp('2020-04-30 00:00:00'),
            pd.Timestamp('2020-05-31 00:00:00'),
            pd.Timestamp('2020-06-30 00:00:00'),
            pd.Timestamp('2020-07-31 00:00:00'),
            pd.Timestamp('2020-08-31 00:00:00')],
        'feature_21': ['RAGXKIMJHFFGKA', '2322341', '!wersrqqw', None, '']
    }

    return pd.DataFrame(test_data)


def test_add_feature31(test_df):

    expected_data = {
        'user_id': [0, 0, 0, 0, 0],
        'month': [
            pd.Timestamp('2020-04-30 00:00:00'),
            pd.Timestamp('2020-05-31 00:00:00'),
            pd.Timestamp('2020-06-30 00:00:00'),
            pd.Timestamp('2020-07-31 00:00:00'),
            pd.Timestamp('2020-08-31 00:00:00')],
        'feature_21': ['RAGXKIMJHFFGKA', '2322341', '!wersrqqw', None, ''],
        'feature_31': ['R', '2', 'w', 'None', 'None']
    }

    expected_df = pd.DataFrame(expected_data)
    calculated_df = add_feature31(test_df)

    assert calculated_df is not None

    # https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.testing.assert_frame_equal.html?highlight=assert
    pd.testing.assert_frame_equal(
        left=calculated_df,
        right=expected_df,
        check_dtype=True,
        check_index_type='equiv',
        check_column_type='equiv',
        check_names=True,
        check_exact=True,
        check_categorical=True,
    )


def test_add_feature31_feature21_does_not_exist():

    df = pd.DataFrame({
        'user_id': [0, 0, 0, 0, 0],
        'month': [
            pd.Timestamp('2020-04-30 00:00:00'),
            pd.Timestamp('2020-05-31 00:00:00'),
            pd.Timestamp('2020-06-30 00:00:00'),
            pd.Timestamp('2020-07-31 00:00:00'),
            pd.Timestamp('2020-08-31 00:00:00')],
        'feature_20': ['RAGXKIMJHFFGKA', '2322341', '!wersrqqw', None, ''],
    })

    with pytest.raises(AttributeError):
        calculated_df = add_feature31(df)


def test_add_feature31_bad_data_type():

    df = pd.DataFrame({
        'user_id': [0, 0, 0, 0, 0],
        'month': [
            pd.Timestamp('2020-04-30 00:00:00'),
            pd.Timestamp('2020-05-31 00:00:00'),
            pd.Timestamp('2020-06-30 00:00:00'),
            pd.Timestamp('2020-07-31 00:00:00'),
            pd.Timestamp('2020-08-31 00:00:00')],
        'feature_21': ['RAGXKIMJHFFGKA', 2322341, '!wersrqqw', None, ''],
    })

    with pytest.raises(TypeError):
        calculated_df = add_feature31(df)
