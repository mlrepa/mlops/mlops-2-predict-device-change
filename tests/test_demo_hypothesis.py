from hypothesis import given
from hypothesis import strategies as st


def backwards_allcaps(text):
    return text.upper()[::-1]


@given(st.text())
def test_backwards_allcaps(input_string):

    modified = backwards_allcaps(input_string)
    assert input_string.upper() == ''.join(reversed(modified))
