# Tutorial: Predict Mobile Device Change

Predicting mobile device change within a month

# Task Description

### Task

- Binary classification
- Target: Predicting the event of changing the mobile device within a month
- Target Metrics: Lift, Precision@K, Recall@K

### Data

Data files:

```
├── raw                         <----- raw data directory
│   ├── target.feather
│   └── user_features.feather
├── scoring_data                <----- directory of scoring dataframes (by month)
└── true_labels                 <----- directory true labels dataframes (by month)
```

**Target: target.feather**

    — user_id - User ID
    — month - Rounding of date to the last day of the month
    — target - 1/0 (1 - user changed the device, 0 - no changes)
    - Reporting dates (month): `2020-01-01` - `2020-12-01`

**Feature Table: user_features.feather**

    — user_id - User ID
    — month - Month for which features are collected
    — feature_N - Specific feature (a total of 50 features, including categorical)
    - Reporting dates (month): `2020-01-01` - `2020-12-01`

**Scoring Data**

Used for simulating model usage in production and monitoring (not used for training)

    - scoring_user_features.feather - Feature table for scoring
    - scoring_target.feather - Target event table
    - Reporting dates (month): `2021-01-01` - `2021-12-01`

**Validation:**

- The model should demonstrate stability over the last 4 months.
- Validation considers the temporal structure, i.e., 4 folds (by month).
- The best model is selected based on averaged metrics across folds - mean, std, min, max.

**Target Metrics**

For the current task, it is assumed that the entire dataset (customer base) can be large, including hundreds of thousands or millions of entries. Therefore, from a business perspective, it makes sense to select the top-K customers with the highest probability of the target event. Consequently, we are interested in having the model perform exceptionally well for this top-K customer subset.

To calculate metrics for top-K (or @k), the generated predictions are first sorted by predict_proba (in descending order), and then the required metric is calculated for these @k objects.

- Precision@K
- Recall@K
- Lift@K - Indicates how much better the model performs compared to a random sample.

For the tutorial, the value of K is set to 5% of the customer base.

## Install & Setup DEV environment

**1. Fork / Clone repo**

- Fork the repository into your personal GitLab account
- Clone the fork repository 

**2. Prepare data storage**

To make the data available to all processes (Gitlab Runner, Airflow DAGs) we need to create
shared storage.

Add storage path to `config/.env`:

```dotenv
STORAGE_PATH=</absolute/path/to/data/storage>
```

examples:

```dotenv
STORAGE_PATH=${PWD}/data  # `data/` directory in the repo root
```

<!-- STORAGE_PATH=${HOME}/storage/tutorial_predict_device_change  # external directory -->

Under `${STORAGE_PATH}` let's create a data storage structure:

```bash
export $(grep -v '#.*' config/.env | xargs)
mkdir -p ${STORAGE_PATH}/{raw,scoring_data,true_labels,scoring_results,monitoring}
```

_Notes:_

- Usually in the repository folder there is a `data` directory for storing data. However, in real projects it is often not possible to store all data in the repository folder and use remote storage (for example, in a separate directory on the same machine)
- In your projects, the STORAGE_PATH path can be specified in `params.yaml`

**3. Download data**

Download data files [>>> from here <<<](https://drive.google.com/file/d/1krOouJz8GI6DPvEBMMS7d-YbJn14vYLH/view?usp=drive_link)
Put (unzip) downloaded data to `${STORAGE_PATH}`

As a result we should have the following folder structure in our storage:

```
${STORAGE_PATH}
    ├── monitoring/                 <- monitoring results data (by month)
    ├── raw/                        <- raw data
    ├── scoring_data/               <- data used for scoring (by month)
    ├── scoring_results/            <- scoring results (by month)
    └── true_labels/                <- true labels for monitoring purposes (by month)
```

In the tutorial's pipeline config we specify paths like:

```yaml
data:
  raw:
    dir_path: raw
    ...
  scoring:
    dir_data_path: scoring_data
    dir_results_path: scoring_results
    dir_true_labels_path: true_labels
```

**4. Build DEV environment (Python Virtual Environment)**
Create and activate virtual environment for `local` develomnet

```bash
python3 -m venv .venv
echo "export PYTHONPATH=$PWD" >> .venv/bin/activate
source .venv/bin/activate
pip install --upgrade pip setuptools wheel
pip install -r requirements.txt
python -m ipykernel install --user --name=mlops-2-predict-device-change
```

**5. Add DVC remote storage (local)**

Add DVC remote path to `config/.env`:

```
DVC_STORAGE=/path/to/dvc/local/storage
```

Create directory which will be used as `DVC` remote (`local` remote `DVC` storage)

```bash
export $(grep -v '#.*' config/.env | xargs)
mkdir -p ${DVC_STORAGE}
```

Add `DVC` remote:

```bash
dvc remote add --local -d local ${DVC_STORAGE}
git add .dvc/config
git commit -m "Setup DVC remote storage ('local')"
```

**Note**: option `--local` saves remote configuration to the Git-ignored local config file
___________________________________________________

## Run ML experiments (pipelines) [DEV environment]

**1. Activate Virtual Environment**
Create and activate virtual environment for `local` development

```bash
source .venv/bin/activate
export $(grep -v '#.*' config/.env | xargs)
```

**2. Run Jupyter Notebooks (if needed)**

```bash
jupyter notebook
```

[http://0.0.0.0:8888/notebooks/](http://0.0.0.0:8888/notebooks/)

**3. Run ML experiments and scoring/monitoring pipelines**

```bash
dvc exp run
```

or create new one with NAME specified:

```bash
dvc exp run -n <NAME> [--set-param <param_name>=<param_value>]
```

**4. Commit Git & DVC changes for success experiment**

```bash
git add .
git commit -m "<Your experiment commit message>"
git push
dvc push
```

**5. Run (test) scoring pipeline**

```bash
python src/pipelines/scoring.py \
    --config=params.yaml \
    --scoring-date='2021-01-01' \
    --storage-path=${STORAGE_PATH}

python src/pipelines/scoring.py \
    --config=params.yaml \
    --scoring-date='2021-02-01' \
    --storage-path=${STORAGE_PATH}
```

**6. Run (test) monitoring pipeline**

```bash
python src/pipelines/model_monitoring.py \
  --config=params.yaml \
  --evaluation-date='2021-02-01' \
  --storage-path=${STORAGE_PATH}
```

___________________________________________________

## MLflow for metrics tracking

**Features**
- To save artifacts (models, images, data) and metadata, we create a separate directory, and the path is specified in the environment variable `MLFLOW_STORAGE`.
- Metrics and metadata are stored in a database (for the tutorial, it's a PostgreSQL database: `mlflow-db`).
- It's more convenient to run MLflow using `docker-compose`.
- When starting an MLflow experiment (`Run`), tags are added - a link to the commit, commit hash, and a link to the tag. In the `tags` table, a record corresponding to the current `Run` is created or updated (if a tag with the specified name already exists).
- The CML report (commit comment) includes a link to the MLflow run.
- `mlflow-server` uses the image `mlrepa/mlflow-server:latest` (see [documentation](https://github.com/mlrepa/mlflow-server)).

***Step 1: Create the MLFLOW_STORAGE Directory**
During MLflow operation, it saves artifact files (models, images, data) to a separate directory. For this tutorial, we will create a directory for MLflow artifacts and specify it in the environment variable `MLFLOW_STORAGE`.
- Create `MLFLOW_STORAGE` within the `STORAGE_PATH` directory.

```bash
export MLFLOW_STORAGE=${STORAGE_PATH}/mlflow-storage
mkdir -p ${MLFLOW_STORAGE}
```

**2. Add environment variables for MLflow to work in `config/.env`**

Variables:
- MLFLOW_STORAGE - хранилище артефактов и метаданных MLflow
- MLFLOW_TRACKING_URI - URI для доступа к MLFlow UI
- MLFLOW_DB_HOST - URI для доступа к PostreSQL DB
- MLFLOW_DB_PORT - порт для доступа к PostreSQL DB
- MLFLOW_DB_NAME - имя базы данных MLflow
- MLFLOW_DB_USER - user name to run MLflow DB
- MLFLOW_DB_PASSWORD - user password to access MLflow DB

Example:

```dotenv
# MLflow
MLFLOW_STORAGE=<absolute/path/to/mlflow-storage>
MLFLOW_TRACKING_URI=http://0.0.0.0:1234
MLFLOW_DB_HOST=mlflow-server
MLFLOW_DB_PORT=5432
MLFLOW_DB_NAME=mlflow-db
MLFLOW_DB_NAME=mlflow
MLFLOW_DB_USER=mlflow
MLFLOW_DB_PASSWORD=mlflow
```

**Note:**
- If you plan to run experiments in a Docker container, you need to modify the `MLFLOW_TRACKING_URI` variable (see step 4 below):
  - `MLFLOW_TRACKING_URI=http://mlflow-server:1234`

**Step 3: Get Docker Images**

To continue working, we need three Docker images:
- `tutorial_predict_device_change` - for running experiments in a container in a PROD environment (Airfow, CI/CD)
- `mlflow-db` - PostgreSQL database for MLflow
- `mlflow-server` - MLflow server for logging metrics and UI

The build and run configuration is specified in the `docker-compose.yaml` file.

Notes:
- When running notebooks and MLflow in Docker, code and data folders are mounted as volumes.
- This allows you to work with the code locally, edit it using any IDE, and the updated code will be available in the container.
- If desired, you can connect to the Python interpreter inside the Docker container.
- Paths specified in `.env` should match the paths inside the Docker container.

*3.1 Get `tutorial_predict_device_change` image*

- Build:
    ```bash
    # Build project and airflow base images
    docker build -t tutorial_predict_device_change:latest \
                -f docker/tutorial_predict/Dockerfile .
    ```

- Pull from `Container registry` of the `Gitlab` repository:
    ```bash
    # Log in to a Docker registry
    docker login -u <user_name> -p <gitlab_access_token>

    # Pull image
    docker pull registry.gitlab.com/mlrepa/mlops-207/tutorial-predict-device-change/tutorial_predict_device_change:latest
    docker tag \
      registry.gitlab.com/mlrepa/mlops-207/tutorial-predict-device-change/tutorial_predict_device_change:latest \
      tutorial_predict_device_change:latest
    ```

- Download:

  - download from <https://disk.yandex.ru/d/O5irej7eiZcbNw>
  - load image:
    ```bash
    docker load -i tutorial_predict_device_change_latest.tar
    ```

*3.2 Run MLflow*
```bash
docker-compose --env-file ./config/.env up
```
После запуска MLflow UI доступен по адресу: 
[http://0.0.0.0:1234/](http://0.0.0.0:1234/)


**4. Run experiments with MLflow Tracking Server**

Running experiments with MLflow differs when running the DVC pipeline in a virtual environment and in a Docker container (based on the `tutorial_predict_device_change_latest` image).

If you are working in a virtual environment (following the instructions above), then the `MLFLOW_TRACKING_URI` environment variable should be set to `http://0.0.0.0:1234`.
- This means that when the script `src/pipelines/log_metrics.py` is run from the host, it attempts to connect to the MLflow Tracking Server at this address.

If you are running experiments in a Docker container, then the value of the `MLFLOW_TRACKING_URI` environment variable should be `http://mlflow-server:1234`.
- This is because Docker containers operate in a virtual network and can "see" each other by the service name specified in `docker-compose.yaml`.
- You need to change the value of this variable in `config/.env`.
- For scenarios with Airflow and CI/CD, the execution occurs in Docker.

When changing the values of environment variables, you may need to export them. You can do this with the command:
```
export $(grep -v '#.*' config/.env | xargs)
```

The basic command to run the experiment pipeline remains the same:
```
dvc exp run
```
___________________________________________

## Run Airflow pipelines (PROD environment)

**1. Build PROD environment (Docker containers)**

```bash
export AIRFLOW_UID=$(id -u)
docker build \
  -t tutorial-predict-airflow-base-monitoring \
  --build-arg AIRFLOW_UID=${AIRFLOW_UID} \
  -f docker/airflow_base/Dockerfile \
  .
```

**2. Setup Airflow Cluster**

Run Airflow-кластер: [`airflow-cluster`](https://gitlab.com/mlrepa/mlops-207/airflow-cluster.git), branch `docker-local-monitoring`

The Airflow cluster consists of the following services:

airflow-db: PostgreSQL database for Airflow.
airflow-webserver: Airflow web server.
airflow-scheduler: Airflow task scheduler.
airflow-init: Airflow initialization service.
monitoring-db: Database service for monitoring.
statsd-exporter: Service for logging Airflow metrics to Prometheus.
prometheus: Service for collecting system metrics.
grafana: Service for visualization and monitoring.


**3. Add the tutorial specific variables**

For Airflow DAGs to work, you need to add environment variables on the Airflow cluster side:

```dotenv
TUTORIAL_PREDICT_REPO_URL=<repo_url>               
TUTORIAL_PREDICT_BRANCH=main
TUTORIAL_PREDICT_REPO_USERNAME=<repo_user_name>
TUTORIAL_PREDICT_REPO_PASSWORD=<repo_passowrd_or_token> 
MLFLOW_TRACKING_URI=http://mlflow-server:1234
MLFLOW_REPORT_PATH=reports/mlflow_report.json
```
*Notes:* 
- TUTORIAL_PREDICT_REPO_URL -  URL to the forked repo 
- TUTORIAL_PREDICT_REPO_PASSWORD - GitLab Personal Access Token (*User Settings -> Access Tokens*)

There are two ways to set variables:

Through the Airflow UI http://localhost:8080/: Admin -> Variables
Upload a .json file (example below, replace with your own values)
Or set each variable individually.

```json
{
  "TUTORIAL_PREDICT_REPO_URL": "<tutorial_predict_default_repo_url>",
  "TUTORIAL_PREDICT_BRANCH": "main",
  "TUTORIAL_PREDICT_REPO_USERNAME": "<repo_user_name>",
  "TUTORIAL_PREDICT_REPO_PASSWORD": "<repo_password_or_token>",
  "MLFLOW_TRACKING_URI": "http://mlflow-server:1234",
  "MLFLOW_REPORT_PATH": "reports/mlflow_report.json"
}
```

1. Via terminal (CLI):

```bash
  # Enter container of airflow scheduler
  docker exec -ti airflow-scheduler /bin/bash

  # Add variable
  airflow variables set <var_name> <value>
```
_______________________

## Develop and run Airflow DAGs

In this tutorial, there are two Airflow DAGs:
- `scoring` - Computes scores (predictions) for clients based on new data.
- `model_monitoring` - Evaluates the accuracy of predictions and stores metrics in a monitoring table.

**Notes:**
- Airflow DAG code is located in the `dags/` directory.
- `scoring` and `model_monitoring` DAGs execute code from the `src/` directory.
- Each time a DAG is run, a separate task clones the repository and switches to the specified branch (default is `main`).

**1. Adding an Environment Variable: AIRFLOW_HOME**

`AIRFLOW_HOME` is the home directory for Airflow, where DAGs, logs, auxiliary code, etc., are stored.

You need to specify this environment variable in the `config/.env` file.

- `AIRFLOW_HOME` - The absolute path to the home directory.
- In the `airflow-cluster` repository, `AIRFLOW_HOME` points to `airflow-cluster/airflow` (refer to the `airflow-cluster` setup instructions).


*Example:*

```dotenv
# Airflow
AIRFLOW_HOME=/home/user/airflow
```

**2. Настроим доставку (copy) Airflow DAGs -> AIRFLOW_HOME**

Copy DAGs from `dags/` to `$AIRFLOW_HOME/dags/` (we will set up automatic delivery in the section about CI/CD)

```bash
export $(grep -v '#.*' config/.env | xargs)
mkdir -p $AIRFLOW_HOME/dags/tutorial_predict_device_change
cp -r dags/* $AIRFLOW_HOME/dags/tutorial_predict_device_change
```

Available DAGs:
- `scoring`: data scoring pipeline
- `model_monitoring`: evaluates scoring results and write metrics to model monitoring table
- `daily_stats`: calculates daily statistics by scoring results



**3. Run scoring & monitoring simulation**

Workflow to run Airflow DAGs:
- run Airflow cluster (see above)
- enter [Airflow UI](http://localhost:8080)
- activate (unpause) DAGs
- trigger DAG

Unpause DAGs `tutorial_predict_device_change.scoring`, `tutorial_predict_device_change.model_monitoring`
and `tutorial_predict_device_change.daily_stats`

There two options to do it:

1. in `Airflow` UI (toggle unpause DAGs)
2. from CLI:

```bash
docker exec -ti airflow-scheduler /bin/bash

airflow dags unpause tutorial_predict_device_change.scoring
airflow dags unpause tutorial_predict_device_change.model_monitoring
airflow dags unpause tutorial_predict_device_change.daily_stats
```

## Setup CI/CD
To set up CI/CD, you need to configure and run the Airflow cluster in advance. During the CI/CD process, DAG code from the dags/ directory is copied to the $AIRFLOW_HOME/dags/tutorial_predict_device_change directory. Additional configurations and environment variables are required.

Requirements:

Running Airflow (airflow-cluster).
Running the MLflow server (project repository).
  

**1. Create Personal Access Token in GitLab**
at GitLab user profile page -> **User Settings** -> Access Tokens
More details: <https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html>

Replace `<gitlab_access_token>` by your created access token in further instructions.


**2. Add Gitlab CI/CD Variables**

1. Go to the Settings -> CI / CD -> Variables page.
2. Click on Expand.
3. Add the following variables:

```dotenv
AIRFLOW_HOME=<рабочая_директория_Airflow>
repo_token=<gitlab_access_token>
STORAGE_PATH=</absolute/path/to/data/storage>
DVC_STORAGE=</absolute/path/to/dvc/remote/storage>
MLFLOW_SERVER=http://mlflow-server:1234
```

*Notes*:
- for `repo_token` the **Masked** flag should be set
- to access variables from any branches, remove the **Protected** flag


**3. Registering and launching GitLab Runner**

Get a token for registration:

1. Go to the page **Settings** -> **CI / CD** -> **Specific Runners** -> **Set up a specific Runner manually**;
2. Copy the registration token from `3.Use the following registration token during setup:`;
3. Declare an environment variable:

    ```bash
    export $(grep -v '#.*' config/.env | xargs)
    export REGISTRATION_TOKEN=<registration_token>
    ```

4. Register gitlab runner (with docker executor)

    ```bash
    gitlab-runner register \
        --non-interactive \
        -u https://gitlab.com/ \
        -r $REGISTRATION_TOKEN \
        --tag-list docker,tutorial_predict_device_change \
        --executor docker \
        --docker-image ghcr.io/iterative/cml:0-dvc2-base1 \
        --docker-disable-cache \
        --docker-volumes "${STORAGE_PATH}:${STORAGE_PATH}" \
        --docker-volumes "${DVC_STORAGE}:${DVC_STORAGE}" \
        --docker-volumes "${AIRFLOW_HOME}:${AIRFLOW_HOME}" \
        --docker-network-mode="mlflow"
    ```

**Notes**:

- _--non-interactive_ - allows you to create a runner in non-interactive mode;
- _-u_ - CI server URL.

5. Launch gitlab runner:

```bash
gitlab-runner run
```

**3. Launch of CI pipeline**

The CI pipeline is implemented in the `.gitlab-ci.yml` file.

The pipeline consists of five stages:

- _build_: building the tutorial image
- _test_: launch autotests
- _mlflow_tags_ - at this stage the task `add_mlflow_tags` is launched
- _train_ - at this stage the `train` task is launched
- _deploy_: deployment (delivery) of DAGs to the host with Airflow

Rules for launching CI pipeline stages:
- _build_, _test_: run sequentially when creating a `Merge Request` from any branch to the **main** branch
- _train_, _mlflow_tags_: run on commit if the commit text contains the substring `[exp]`
- _deploy_: with `Merge Request` in **main**
  
To run `build`, `test` and `train` CI jobs:
- enter the repository at GitLab
- create a new branch from the `main`branch and make any update
- create a `Merge Request` to the `main` branch
- or, make a commit to any branch with commit message containing '[exp]' string 

To run `deploy_dags` CI job:
- merge the request to `main` branch


**4. Как работает деплой (доставка) DAG-ов**

- в папке `dags` в домашней директории Airflow (${AIRFLOW_HOME}) создаётся папка с именем проекта
- CI-задача `deploy` (`.gitlab-ci.yml`): копирует содержимое папки `./dags` репозитория в эту папку
  проекта:

```
export DAGS_FOLDER=${AIRFLOW_HOME}/dags/${PROJECT_FOLDER}

# Create ${DAGS_FOLDER}
rm -rf ${DAGS_FOLDER} && mkdir -p ${DAGS_FOLDER}

# Copy content of folder ./dags to folder with the project name
# inside directory ${DAGS_FOLDER}
cp -r dags/* ${DAGS_FOLDER}
```
