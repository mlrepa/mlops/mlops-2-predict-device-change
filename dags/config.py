from airflow.models import Variable
import os

from tutorial_predict_device_change.utils.utils import generate_dag_run_path, extract_repo_name

# [Define custom variables]
project_args = {
    'ts': "{{ ts_nodash }}",  # execution date in '20180101T000000' format
    'airflow_run_dir': os.getenv('AIRFLOW_RUN_DIR'),
    'repo_name': extract_repo_name(Variable.get('TUTORIAL_PREDICT_REPO_URL')),
    'repo_url': Variable.get('TUTORIAL_PREDICT_REPO_URL'),
    'repo_username': Variable.get('TUTORIAL_PREDICT_REPO_USERNAME'),
    'repo_password': Variable.get('TUTORIAL_PREDICT_REPO_PASSWORD'),
    'branch': Variable.get('TUTORIAL_PREDICT_BRANCH'),
    'mlflow_tracking_uri': Variable.get('MLFLOW_TRACKING_URI'),
    'mlflow_report_path': Variable.get('MLFLOW_REPORT_PATH'),
    'monitoring_db_name': os.getenv('MONITORING_DB_NAME'),
    'monitoring_db_user': os.getenv('MONITORING_DB_USER'),
    'monitoring_db_password': os.getenv('MONITORING_DB_PASSWORD'),
    'monitoring_db_host': os.getenv('MONITORING_DB_HOST'),
    'monitoring_db_port': os.getenv('MONITORING_DB_PORT'),
    'storage_path': os.getenv('TUTORIAL_PREDICT_STORAGE'),
    'dvc_remote_storage': os.getenv('TUTORIAL_PREDICT_DVC_STORAGE')
}

project_args['dag_run_dir'] = generate_dag_run_path(
    project_args['airflow_run_dir'],
    project_args['repo_name'],
    ts_macros=project_args['ts']
)

