from airflow import DAG
from airflow.sensors.external_task import ExternalTaskSensor
from datetime import timedelta
from dateutil.relativedelta import relativedelta
import pandas as pd
from pandas.tseries.offsets import MonthBegin
from pathlib import Path
import psycopg2
from typing import Text

from tutorial_predict_device_change.config import project_args
from tutorial_predict_device_change.utils.datetime import (
    first_month_day, prev_day
)

from tutorial_predict_device_change.utils.utils import calc_stats


default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': '2020-04-02',
    'end_date': '2021-07-01',
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5)
}

dag = DAG(
    'tutorial_predict_device_change.daily_stats',
    default_args=default_args,
    description='Calculate daily statistics',
    schedule_interval='@daily',
    max_active_runs=1
)


with dag:

    wait_model_monitoring = ExternalTaskSensor(
        task_id='wait_model_monitoring',
        external_dag_id='tutorial_predict_device_change.model_monitoring',
        external_task_id='run_model_monitoring',
        allowed_states=['success'],
        failed_states=['failed', 'skipped'],
        execution_date_fn=lambda exec_date: exec_date - relativedelta(day=6),
        poke_interval=30
    )

    @dag.task()
    def statistics(run_date: Text):
        """Запускает расчёт статистика на определённую дату
        Args:
            run_date: дата запуска пайплайна, в формате ISO

        Returns:

        """
        connection = psycopg2.connect(
            dbname=project_args.get('monitoring_db_name'),
            user=project_args.get('monitoring_db_user'),
            password=project_args.get('monitoring_db_password'),
            host=project_args.get('monitoring_db_host'),
            port=project_args.get('monitoring_db_port')
        )
        cursor = connection.cursor()
        cursor.execute(
            f'''
                CREATE TABLE IF NOT EXISTS tutorial_predict_daily_stats(
                    timestamp BIGINT,
                    key TEXT,
                    value REAL
                );
            '''
        )

        # Defines dates
        yesterday = prev_day(run_date)
        month_start = first_month_day(yesterday)
        print(f'month_start={month_start}')
        # next_month = date.fromisoformat(month_start) + relativedelta(months=1, day=1)
        ts = int(pd.to_datetime(yesterday).timestamp() * 1000)

        # Загружает скоринговый датасет с реальными значениями целевой переменной (generated every day)
        storage_path = Path(project_args.get('storage_path'))
        true_labels_path = storage_path / 'true_labels' / f'{month_start}.feather'
        true_labels = pd.read_feather(true_labels_path)
        true_labels_month = (true_labels.loc[
                            (true_labels['date'] >= pd.to_datetime(month_start))
                            & (true_labels['date'] <= pd.to_datetime(yesterday))
                            & true_labels['label_true'] == 1]
                        .reset_index(drop=True)
                        .assign(month=month_start)
                        )
        true_labels_month['month'] = pd.to_datetime(true_labels_month['month'])
        # Загружаем датасет с результатами скоринга за указанный месяц
        month_scoring_data_path = storage_path / 'scoring_results' / f'{month_start}.feather'
        scores_month = pd.read_feather(month_scoring_data_path)
        scores_month['scoring_date'] = pd.to_datetime(scores_month['scoring_date'])
        scores_month['scoring_date'] = scores_month['scoring_date'] + MonthBegin(1)

        # Считаем необходимые статистики и метрики
        stats = calc_stats(scores_month, true_labels_month, day=yesterday)
        print(stats)

        for k, v in stats.items():
            cursor.execute(
                f'''
                    INSERT INTO tutorial_predict_daily_stats(
                        timestamp, key, value
                    ) VALUES(%s, %s, %s)
                ''',
                (ts, k, v)
            )

        connection.commit()
        connection.close()


    statistics = statistics('{{ ds }}')
    
    wait_model_monitoring >> statistics
