from datetime import date
from pathlib import Path
from typing import Text


def generate_dag_run_path(base_run_dir: Text, repo_name: Text, ts_macros: Text) -> Text:
    """Generate path to temporary directory for DAG running
    Args:
        base_run_dir: Directory used to create temporary dirs to run DAG
        repo_name: Project repo name
        ts_macros: Execution date in '20180101T000000' format

    Returns: Path to tmp run directory

    """
    dag_run_path = Path(base_run_dir) / str(ts_macros) / repo_name
    print(f"Path to tmp run directory: {dag_run_path}")

    return str(dag_run_path)


def create_dag_run_dir(dag_run_dir: Text) -> bool:
    """Create temporary directory for DAG running
    Args:
        dag_run_dir {Text}: path to local repository
    Returns:
        bool: True if ok
    """
    print(f"Create temporary directory: {dag_run_dir}")
    Path(dag_run_dir).mkdir(exist_ok=True, parents=True)
    return True


def extract_repo_name(repo_url: Text) -> Text:
    """Extracts repository name from its URL.
    Args:
        repo_url {Text}: repository URL
    Returns:
        Text: repository name
    """

    return repo_url.split('/')[-1].replace('.git', '')


def repo_url_with_credentials(repo_url: Text, user: Text, password: Text) -> Text:
    """Generate repo URL with credentials user:password
    Args:
        repo_url {Text}: repository URL
        user {Text}: repository username
        password {Text}: repository password or access token
    Returns:
        Text: repository URL with credentials:
            https://<user>:<password>@gitlab.com/group/project.git
    """

    creds = f'{user}:{password}@'
    schema_len = len('https://')
    repo_url_with_creds = repo_url[:schema_len] + creds + repo_url[schema_len:]
    return repo_url_with_creds


def calc_stats(scores_month, target_month, day: Text):
    """Считает статистики за указанную дату и записывает их в базу данных

    Args:
        run_date {Text}: дата, на которую производится расчёт статистик, в формате ISO

    Returns:

    """

    # Statistics
    user_base_size = scores_month['user_id'].nunique()

    # Count customers who changed device
    changes_count = target_month[(target_month['date'] == day)]['user_id'].nunique()
    changes_count_cumsum = target_month.label_true.sum()

    # Device change statistics
    day_of_month = date.fromisoformat(day).day
    avg_changes_per_day = changes_count_cumsum / day_of_month
    change_rate_day = changes_count / user_base_size
    change_rate_month = changes_count_cumsum / user_base_size

    # Metrics
    scoring_target = scores_month.merge(target_month,
                                        left_on=['user_id', 'scoring_date'],
                                        right_on=['user_id', 'month'],
                                        )

    # Кол-во верно предсказанных пользователей, сменивших девайс
    scoring_target['tp'] = scoring_target['label_true'] * scoring_target['label']
    metrics = (scoring_target.copy()
        .groupby('date')
        .agg({'label_true': 'sum',
           'label': 'sum',
           'tp': 'sum'})
        .assign(recall=lambda df: df.tp / df.label_true)
        .reset_index()
        .loc[lambda x: x.date == day].iloc[0]
        ).to_dict()

    return {
        'user_base_size': user_base_size,
        'changes_count': changes_count,
        'changes_count_cumsum': changes_count_cumsum,
        'avg_changes_per_day': avg_changes_per_day,
        'change_rate_day': change_rate_day,
        'change_rate_month': change_rate_month,
        'label_true_count': metrics['label_true'],
        'tp': metrics['tp'],
        'recall': metrics['recall']
    }
