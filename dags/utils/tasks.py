from datetime import datetime
import git
import json
from pathlib import Path
import shutil
from typing import Text

from tutorial_predict_device_change.utils.utils import repo_url_with_credentials


def clone_repo_task(repo_url: Text,
                    branch: Text,
                    repo_local_path: Text,
                    repo_username: Text,
                    repo_password: Text,
                    ) -> None:
    """Clone Git repo

    Args:
        repo_url: Remote Git repo URL
        branch: Target branch  to source code
        repo_local_path: Local directory for DAG running
        repo_username: Username
        repo_password: Password (personal access token

    Returns: None

    """
    print(f'Cloning {repo_url}')
    print(f'Repo clone to {repo_local_path}')
    repo_url_with_creds = repo_url_with_credentials(repo_url, repo_username, repo_password)
    repo_local_path = Path(repo_local_path)

    if repo_local_path.exists():
        try:
            shutil.rmtree(repo_local_path)
        except OSError as e:
            print("Error: %s - %s." % (e.filename, e.strerror))

    git.Repo.clone_from(repo_url_with_creds, repo_local_path)
    repo = git.Repo(repo_local_path)
    repo.git.checkout(branch)
    repo.git.pull('origin', branch)

    print(f'Repository cloned to: {repo_local_path}')


def commit_experiment_task(repo_local_path: Text, branch: Text, mlflow_report_path: Text) -> None:
    """Do Git commit experiment results & push to target remote repo branch
    Args:
        repo_local_path: Local directory for DAG running
        branch: Target branch to commit experiment results
        mlflow_report_path: relative path to mlflow report inside repository
    """

    report = None
    exp_name = None
    run_id = None

    repo = git.Repo(repo_local_path)
    repo_changed = len(repo.git.diff()) > 0

    if repo_changed:

        print(f'Parse {mlflow_report_path} to get MLflow Experiment name & Run ID')
        repo_local_path = Path(repo_local_path)
        report_path = repo_local_path / mlflow_report_path
        print(f'report_path = {report_path}')

        if report_path.is_file():
            with open(report_path) as report_f:
                report = json.load(report_f)
                exp_name = report.get('exp_name')
                run_id = report.get('run_id')
        else:
            print(f"No mlflow report found by path: {mlflow_report_path}")

        print(f'MLFLOW_REPORT.MD - report: {report}')
        print(f'MLFLOW_REPORT.MD - exp_name: {exp_name}')
        print(f'MLFLOW_REPORT.MD - run_id: {run_id}')

        print('Commit start')
        commit_datetime = datetime.utcnow().strftime('%Y-%m-%d-%H-%M-%S')
        exp_name = exp_name.replace(' ', '-')
        tag_name = commit_message = f'exp-{exp_name}-{run_id}-{commit_datetime}'
        repo.git.add(u=True)
        repo.git.commit('-m', commit_message)
        repo.create_tag(tag_name, message=commit_message)
        repo.git.push('origin', branch)
        repo.git.push('origin', tag_name)
        print(f'Commit end')
        print(f'Remote branch updated: {branch}')
        print(f'Tag added to commit: {tag_name}')

    else:
        print(f'No repo changes. Nothing to commit! Repo changes count: {repo_changed}')

