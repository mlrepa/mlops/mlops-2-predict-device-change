from datetime import date
from dateutil.relativedelta import relativedelta
import logging
import pandas as pd
from pandas._libs.tslibs.offsets import MonthEnd
from pandas.tseries.offsets import MonthBegin
import re
from typing import Text


logging.basicConfig(level=logging.INFO)


def first_month_day(dt: Text) -> Text:
    """Get date of the first month day.
    Args:
        dt {Text}: date in ISO format (YYYY-MM-DD)
    Returns:
        Text: date in ISO format
    """

    d = date.fromisoformat(dt).replace(day=1)

    return d.strftime('%Y-%m-%d')


def last_month_day(dt: Text) -> Text:
    """Get date of the lat month day.
    Args:
        dt {Text}: date in ISO format (YYYY-MM-DD)
    Returns:
        Text: date in ISO format
    """

    d = date.fromisoformat(dt)
    last_day = d + relativedelta(day=31)
    return last_day.strftime('%Y-%m-%d')


def prev_day(dt: Text) -> Text:
    """Get date of previous day.
    Args:
        dt {Text}: date in ISO format (YYYY-MM-DD)
    Returns:
        Text: date in ISO format
    """

    return (date.fromisoformat(dt) + relativedelta(days=-1)).strftime('%Y-%m-%d')


def get_scoring_date(dt: Text) -> Text:
    """Get first day of the scoring period (month)
    Args:
        dt {Text}: date in ISO format (YYYY-MM-DD)
    Returns:
        Text: date in ISO format
    """

    tr_month_start = pd.to_datetime(dt) + MonthBegin(1)
    return first_month_day(tr_month_start.strftime('%Y-%m-%d'))


def get_train_period_end(scoring_date: Text, delay: int = 1) -> Text:
    """Get last date of the available training data
    Args:
        dt {Text}: date in ISO format (YYYY-MM-DD)
        delay: time period (in months) between last train data point available and scoring period start
    Returns:
        Text: date in ISO format
    """

    tr_month_start = pd.to_datetime(scoring_date) - MonthEnd(delay+1)
    return last_month_day(tr_month_start.strftime('%Y-%m-%d'))


def get_evaluate_period_date(dt: Text, delay: int = 1) -> Text:
    """Get date of the lat month day.
    Args:
        dt {Text}: date in ISO format (YYYY-MM-DD)
    Returns:
        Text: date in ISO format
    """

    tr_month_start = pd.to_datetime(dt) - MonthEnd(delay)
    return first_month_day(tr_month_start.strftime('%Y-%m-%d'))


def check_isoformat(date: Text) -> None:
    """Checks date type and format.
    Args:
        date {Text}: date
    Raises:
        TypeError: if date type is not string
        ValueError: if date string is not in ISO format (YYYY-MM-DD)
    """

    if not isinstance(date, str):
        raise TypeError(f'Invalid date type: {type(date)}, must be string')

    if not re.match(r'^\d{4}-\d\d-\d\d', date):
        raise ValueError('Bad date format. ISO format required: YYYY-MM-DD')


def extract_train_period_end(ds: Text, dag_run_train_period_end: Text = None) -> Text:
    """Extracts train period end.
    Args:
        ds {Text}: the execution date as YYYY-MM-DD
        dag_run_train_period_end {Text}: train period end from dag_run.conf
    Returns:
        Text: date in ISO format (YYYY-MM-DD)
    """

    train_period_end = dag_run_train_period_end

    if not train_period_end:
        train_period_end = get_train_period_end(ds, 0)

    check_isoformat(train_period_end)

    return train_period_end


def extract_scoring_date(ds: Text, dag_run_scoring_date: Text = None) -> Text:
    """Extracts train period end.
    Args:
        ds {Text}: the execution date as YYYY-MM-DD
        dag_run_scoring_date {Text}: scoring date from dag_run.conf
    Returns:
        Text: date in ISO format (YYYY-MM-DD)
    """

    scoring_date = dag_run_scoring_date

    if not scoring_date:
        scoring_date = get_scoring_date(ds)

    check_isoformat(scoring_date)

    return scoring_date


def extract_evaluation_date(ds: Text, dag_run_evaluation_date: Text = None) -> Text:
    """Extracts train period end.
    Args:
        ds {Text}: the execution date as YYYY-MM-DD
        dag_run_evaluation_date {Text}: evaluation date from dag_run.conf
    Returns:
        Text: date in ISO format (YYYY-MM-DD)
    """

    evaluation_date = dag_run_evaluation_date

    if not evaluation_date:
        evaluation_date = get_evaluate_period_date(ds)

    check_isoformat(evaluation_date)

    return evaluation_date
