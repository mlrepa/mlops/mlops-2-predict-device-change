from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.sensors.filesystem import FileSensor
from airflow.sensors.external_task import ExternalTaskSensor
from datetime import timedelta
from dateutil.relativedelta import relativedelta
import os
from pathlib import Path
import shutil
from typing import Text

from tutorial_predict_device_change.config import project_args
from tutorial_predict_device_change.utils.tasks import clone_repo_task
from tutorial_predict_device_change.utils.utils import create_dag_run_dir
from tutorial_predict_device_change.utils.datetime import first_month_day

# [Define default DAG arguments]
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': '2020-04-01',
    'end_date': '2021-07-01',
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5)
}

dag = DAG(
    'tutorial_predict_device_change.model_monitoring',
    default_args=default_args,
    description='Make scoring evaluation',
    schedule_interval='0 0 6 * *',
    max_active_runs=1,
    catchup=True,
    user_defined_macros={
        'first_month_day': first_month_day
    }
)

with dag:

    # [Implement task definitions]

    wait_scoring = ExternalTaskSensor(
        task_id='wait_scoring',
        external_dag_id='tutorial_predict_device_change.scoring',
        external_task_id='run_scoring',
        allowed_states=['success'],
        failed_states=['failed', 'skipped'],
        execution_date_fn=lambda exec_date: exec_date - relativedelta(months=1, days=1),
        poke_interval=30
    )

    check_true_labels = FileSensor(
        task_id='check_true_labels',
        filepath = os.path.join(
            project_args.get('storage_path'),
            'true_labels',
            '{{ first_month_day(ds) }}.feather'
        ),
        timeout=10
    )

    @dag.task()
    def create_tmp_dir(dag_run_dir: Text):
        """"Creates temporary folder (of local repository) in $AIRFLOW_RUN_DIR, from which
        scoring script will be run.
        Args:
            dag_run_dir: local repository path; dag_run_dir = $AIRFLOW_RUN_DIR/<timestamp>/<repo_name>
        """
        create_dag_run_dir(dag_run_dir)


    @dag.task()
    def clone(dag_run_dir: Text):
        """Clones repository in dag_run_dir, switches on specified branch.
        Args:
            dag_run_dir: local repository path; dag_run_dir = $AIRFLOW_RUN_DIR/<timestamp>/<repo_name>
        """
        clone_repo_task(
            repo_url=project_args.get('repo_url'),
            branch=project_args.get('branch'),
            repo_local_path=dag_run_dir,
            repo_username=project_args.get('repo_username'),
            repo_password=project_args.get('repo_password')
        )


    run_model_monitoring = BashOperator(
        task_id='run_model_monitoring',
        bash_command=f'''
            cd {project_args.get('dag_run_dir')} && \
            export PYTHONPATH=. && \
            export MONITORING_DB_NAME={project_args.get('monitoring_db_name')} && \
            export MONITORING_DB_USER={project_args.get('monitoring_db_user')} && \
            export MONITORING_DB_PASSWORD={project_args.get('monitoring_db_password')} && \
            export MONITORING_DB_HOST={project_args.get('monitoring_db_host')} && \
            export MONITORING_DB_PORT={project_args.get('monitoring_db_port')} && \
            python src/pipelines/model_monitoring.py \
                --config=params.yaml \
                --evaluation-date={{{{ first_month_day(ds) }}}} \
                --storage-path={project_args.get('storage_path')} \
                --db
        '''
    )

    @dag.task(trigger_rule='all_success')
    def clean(dag_run_dir):
        """
        Removes the repository temporary folder.
        Args:
            dag_run_dir: local repository path; dag_run_dir = $AIRFLOW_RUN_DIR/<timestamp>/<repo_name>
        """

        shutil.rmtree(Path(dag_run_dir).parent)


    # [Create task instances]
    dag_run_dir = project_args.get('dag_run_dir')
    create_tmp_dir = create_tmp_dir(dag_run_dir)
    clone = clone(dag_run_dir)
    clean = clean(dag_run_dir)

    # [Set task dependencies]
    [wait_scoring, check_true_labels] >> create_tmp_dir >> clone >> run_model_monitoring >> clean
