from airflow import DAG
from airflow.operators.bash import BashOperator
from datetime import timedelta
from pathlib import Path
import shutil
from typing import Text

from tutorial_predict_device_change.config import project_args
from tutorial_predict_device_change.utils.tasks import clone_repo_task
from tutorial_predict_device_change.utils.utils import create_dag_run_dir
from tutorial_predict_device_change.utils.datetime import first_month_day

# [Define default DAG arguments]
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': '2020-03-01',
    'end_date': '2021-07-01',
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
}

# [Crate DAG instance]
dag = DAG(
    'tutorial_predict_device_change.scoring',
    default_args=default_args,
    description='Make scoring',
    schedule_interval='0 0 5 * *',
    max_active_runs=1,
    catchup=True,
    user_defined_macros={
        'first_month_day': first_month_day
    }
)

with dag:

    # [Implement task definitions]
    @dag.task()
    def create_tmp_dir(dag_run_dir: Text):
        """Creates temporary folder (of local repository) in $AIRFLOW_RUN_DIR, from which
        scoring script will be run.
        Args:
            dag_run_dir: local repository path; dag_run_dir = $AIRFLOW_RUN_DIR/<timestamp>/<repo_name>
        """
        create_dag_run_dir(dag_run_dir)

    @dag.task()
    def clone(dag_run_dir: Text):
        """Clones repository in dag_run_dir, switches on specified branch.
        Args:
            dag_run_dir: local repository path; dag_run_dir = $AIRFLOW_RUN_DIR/<timestamp>/<repo_name>
        """
        clone_repo_task(
            repo_url=project_args.get('repo_url'),
            branch=project_args.get('branch'),
            repo_local_path=dag_run_dir,
            repo_username=project_args.get('repo_username'),
            repo_password=project_args.get('repo_password')
        )

    run_scoring = BashOperator(
        task_id='run_scoring',
        bash_command=f'''
            cd {project_args.get('dag_run_dir')} && \
            export PYTHONPATH=. && \
            dvc remote add --local -d local {project_args.get('dvc_remote_storage')} && \
            dvc fetch && \
            dvc checkout && \
            python src/pipelines/scoring.py \
                --config=params.yaml \
                --scoring-date={{{{ first_month_day(ds) }}}} \
                --storage-path={project_args.get('storage_path')}
        '''
    )

    @dag.task(trigger_rule='all_success')
    def clean(dag_run_dir):
        """
        Removes the repository temporary folder.
        Args:
            dag_run_dir: local repository path; dag_run_dir = $AIRFLOW_RUN_DIR/<timestamp>/<repo_name>
        """

        shutil.rmtree(Path(dag_run_dir).parent)

    # [Create task instances]
    dag_run_dir = project_args.get('dag_run_dir')
    create_tmp_dir = create_tmp_dir(dag_run_dir)
    clone = clone(dag_run_dir)
    clean = clean(dag_run_dir)

    # [Set task dependencies]
    create_tmp_dir >> clone >> run_scoring >> clean
