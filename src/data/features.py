import pandas as pd
import re


def add_feature31(df: pd.DataFrame) -> pd.DataFrame:
    """Adds some first letter device code
    Args:
        df {pandas.DataFrame}: dataframe
    Returns:
        pandas.DataFrame
    """

    df['feature_31'] = df.feature_21.apply(lambda s: 'None' if s in ['', None] else re.findall(r'[\w]', s)[0])

    return df
