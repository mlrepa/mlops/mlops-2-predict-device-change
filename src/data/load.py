import pandas as pd
from typing import Text


def load_target(path: Text) -> pd.DataFrame:
    """Loads target dataset.
    Args:
        path {Text}: path to dataset
    Returns:
        pandas.DataFrame
    """
    return pd.read_feather(path)


def load_data(path: Text) -> pd.DataFrame:
    """Loads dataset.
    Args:
        path {Text}: path to dataset
    Returns:
        pandas.DataFrame
    """
    return pd.read_feather(path)