import pandas as pd

from src.data.features import add_feature31


def process_target(target_df: pd.DataFrame) -> pd.DataFrame:
    """Processes target values.
    Args:
        target_df {pandas.DataFrame}: target dataset
    Returns:
        pandas.DataFrame: dataset with processed target values
    """

    target_df['month'] = pd.to_datetime(target_df['month'])
    target_df.drop('date', axis=1, inplace=True)

    return target_df


def process_data(data_df: pd.DataFrame) -> pd.DataFrame:
    """Processes dataset.
    Args:
        data_df {pandas.DataFrame}: dataset
    Returns:
        pandas.DataFrame: processed dataset
    """

    data_df['month'] = pd.to_datetime(data_df['month'])

    return data_df


def process_features(features_df: pd.DataFrame) -> pd.DataFrame:
    features_df = process_data(features_df)
    features_df = add_feature31(features_df)

    return features_df
