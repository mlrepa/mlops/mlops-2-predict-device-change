"""This script extracts features from raw data."""

import argparse
import pandas as pd
from pathlib import Path
from typing import Text

from src.data.load import load_target, load_data
from src.data.process import process_features
from src.utils.config import load_config
from src.utils.logging import get_logger


def featurize(config_path: Text) -> None:
    """Extract features and save it.
    Args:
        config_path {Text}: path to config file
    """

    config = load_config(config_path)
    logger = get_logger("FEATURIZE", config.base.log_level)

    logger.info('Load dataset')
    processed_data_dir = Path(config.data.processed.dir_path)
    target_df = load_target(processed_data_dir / config.data.processed.target)
    features_df = load_data(processed_data_dir / config.data.processed.user_features)

    logger.info('Filter dataset rows')
    features_df = features_df.loc[features_df.user_id.isin(target_df.user_id), ]

    logger.info('Process dataset')
    features_df = process_features(features_df)

    logger.info('Add target column')
    features = features_df.copy()
    features = pd.merge(
        left=features,
        right=target_df,
        how='left',
        on=['user_id', 'month']
    )

    logger.info('Process nulls')
    features.dropna(inplace=True)

    logger.info('Save features')
    features_path = processed_data_dir / config.data.processed.features
    features.to_feather(features_path)
    logger.debug(f'Features path: {features_path}')


if __name__ == '__main__':

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()

    featurize(config_path=args.config)
