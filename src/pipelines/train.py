"""This script trains model."""

import argparse
from catboost import CatBoostClassifier
from category_encoders import OrdinalEncoder
import joblib
import json
import pandas as pd
from pathlib import Path
import pickle
from sklearn.ensemble import RandomForestClassifier
from typing import Text

from src.evaluate.metrics import precision_at_k_score, recall_at_k_score, lift_score
from src.train.train import custom_ts_split, get_split_data
from src.utils.datetime_utils import check_isoformat
from src.utils.errors import UnknownEstimatorError
from src.utils.plot import dataframe_to_image, build_lift_curve
from src.utils.config import load_config
from src.utils.logging import get_logger


def train(config_path: Text) -> None:
    """Train model.
    Args:
        config_path {Text}: path to config file
    """

    config = load_config(config_path)
    train_period_end = config.base.run.train_period_end
    check_isoformat(train_period_end)

    print(config.base)

    logger = get_logger("TRAIN", config.base.log_level)
    logger.info('load data')

    # Load data
    processed_data_dir = Path(config.data.processed.dir_path)
    features_path = processed_data_dir / config.data.processed.features
    features = pd.read_feather(features_path)
    features['month'] = pd.to_datetime(features['month'])
    print(f'features size:{features.shape}')
    print(f'features months: {features.month.unique()}')
    features = features[
        (features['month'] <= pd.to_datetime(train_period_end))
    ].reset_index(drop=True)

    estimator = config.train.estimator

    logger.info(f'Estimator = {estimator}')
    logger.info('Fit')

    if estimator == 'catboost':
        algorithm = CatBoostClassifier
    elif estimator == 'random_forest':
        algorithm = RandomForestClassifier
    else:
        raise UnknownEstimatorError(f'Unknown estimator: {estimator}')

    estimator_params = config.train.estimators[estimator]
    clf = algorithm(**estimator_params, random_state=config.base.random_state)

    encoder = OrdinalEncoder()
    categories = config.featurize.categories
    encoder.fit(features[categories].drop_duplicates())

    metrics_df = pd.DataFrame(columns=['test_period', 'lift', 'precision_at_k', 'recall_at_k'])
    top_K = int(features.shape[0] * config.train.top_K_coef)
    months = features.month.sort_values().unique()
    print(months)
    k = 1

    logger.info(f'Top_K is 5.0 % of dataset_size: {top_K}')

    y_test = []
    y_pred = []

    for start_train, end_train, test_period in custom_ts_split(months, train_period=1):

        logger.info(f'Fold {k}:')
        logger.info(f'Train: {start_train} - {end_train}')
        logger.info(f'Test: {test_period} \n')

        # Get train / test data for the split
        X_train, X_test, y_train, y_test = get_split_data(features, start_train, end_train, test_period)
        X_train[categories] = encoder.transform(X_train[categories])
        X_test[categories] = encoder.transform(X_test[categories])
        logger.info(f'Train shapes: X - {X_train.shape}, y - {y_train.shape}')
        logger.info(f'Test shapes: X - {X_test.shape}, y - {y_test.shape}')

        # Fit estimator
        clf.fit(X_train, y_train)
        y_pred = clf.predict(X_test)
        probas = clf.predict_proba(X_test)
        logger.info(f'Max probas: {probas[:, 1].max()}')

        lift = lift_score(y_test, y_pred, probas[:, 1], top_K)
        precision_at_k = precision_at_k_score(y_test, y_pred, probas[:, 1], top_K)
        recall_at_k = recall_at_k_score(y_test, y_pred, probas[:, 1], top_K)
        
        """
        Method `append()` deprecated since version 1.4.0:
        https://pandas.pydata.org/pandas-docs/version/1.4/reference/api/pandas.DataFrame.append.html

        Here the method `_append()` is used as a temporary workaround
        """
        metrics_df = metrics_df._append(
            dict(zip(metrics_df.columns, [test_period, lift, precision_at_k, recall_at_k])),
            ignore_index=True
        )

        k += 1
        logger.info(f'Precision at {top_K}: {precision_at_k}')
        logger.info(f'Recall at {top_K}: {recall_at_k}\n')

    logger.info('Build and save lift curve')
    lift_curve_fig = build_lift_curve(y_test[:top_K], y_pred[:top_K], step=config.train.lift_curve_step)
    lift_curve_fig.savefig(config.train.lift_curve_image)

    logger.info('Save "raw" metrics for plotting')
    metrics_df.to_csv(config.train.raw_metrics_path, index=False)

    logger.info('Save aggregated metrics')
    metrics_aggs = metrics_df[['lift', 'precision_at_k', 'recall_at_k']].agg(['max', 'min', 'std', 'mean'])
    metrics = {
        f'{metric}_{agg}': metrics_aggs.loc[agg, metric]
        for metric in metrics_aggs.columns
        for agg in metrics_aggs.index
    }
    with open(config.train.train_metrics, 'w') as metrics_f:
        json.dump(obj=metrics, fp=metrics_f, indent=4)

    logger.info('Generate & Save plots')
    plots_df = pd.DataFrame({
        'metric': list(metrics.keys()),
        'value': list(metrics.values())
    })
    plots_df.to_csv(config.train.train_plots_path, index=False)

    dataframe_to_image(metrics_aggs, config.train.train_metrics_image)

    logger.info('Save model')
    model_path = config.train.model_path
    joblib.dump(clf, model_path)
    logger.info(f'Model saved to: {model_path}')

    logger.info('Log encoder')
    with open(config.train.encoder_path, 'wb') as ef:
        pickle.dump(encoder, ef)


if __name__ == '__main__':

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()

    train(config_path=args.config)
