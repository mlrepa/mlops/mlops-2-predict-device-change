"""This script makes model monitoring: evaluates results of scoring."""

import argparse
import os
import pandas as pd
from pathlib import Path
import psycopg2
from typing import Text

from src.utils.datetime_utils import check_isoformat
from src.utils.config import load_config
from src.utils.logging import get_logger
from src.evaluate.metrics import lift_score, precision_at_k_score, recall_at_k_score


def model_monitoring(
        config_path: Text,
        evaluation_date: Text,
        storage_path: Text,
        db: bool = False
) -> None:
    """Scores data on trained model.
    Args:
        config_path {Text}: path to config file
        evaluation_date {Text}: date on which evaluation is executed
        storage_path {Text}: path to data storage
        db {bool}: whether to write metrics to database flag
    """

    config = load_config(config_path)
    logger = get_logger('MODEL_MONITORING', config.base.log_level)
    check_isoformat(evaluation_date)
    logger.info(f'evaluation_date = {evaluation_date}')

    logger.info('Load dataset')

    if not storage_path:
        logger.warning(f'Storage path is not set. Default value will be used')
        storage_path = './data'

    storage_path = Path(storage_path)
    logger.info(f'Storage path = {storage_path}')

    true_labels_dir = storage_path / config.data.scoring.dir_true_labels_path
    true_labels = pd.read_feather(true_labels_dir / f'{evaluation_date}.feather')
    true_labels = true_labels[['user_id', 'month', 'label_true']]

    dir_scoring_results = storage_path / config.data.scoring.dir_results_path
    scores_df_path = dir_scoring_results / f'{evaluation_date}.feather'
    scores_df = pd.read_feather(scores_df_path)
    scores_df['scoring_date'] = pd.to_datetime(scores_df['scoring_date'])
    scores_df = scores_df.merge(true_labels, on=['user_id'])
    top_K = int(true_labels.shape[0] * config.train.top_K_coef)

    logger.info('Calculate Metrics')
    y_test = scores_df['label_true']
    y_pred = scores_df['label']
    probas = scores_df['score']

    lift = lift_score(y_test, y_pred, probas, top_K)
    precision_at_k = precision_at_k_score(y_test, y_pred, probas, top_K)
    recall_at_k = recall_at_k_score(y_test, y_pred, probas, top_K)

    metrics_df = pd.DataFrame(columns=['month', 'lift', 'precision_at_k', 'recall_at_k'])
    metrics_df = metrics_df.append(
        dict(zip(metrics_df.columns, [evaluation_date, lift, precision_at_k, recall_at_k])),
        ignore_index=True
    )

    logger.info('Save monitoring metrics')
    dir_monitoring_results = storage_path / config.data.scoring.dir_monitoring_path
    report_path = dir_monitoring_results / f'{evaluation_date}.csv'
    metrics_df.to_csv(report_path, index=False)

    if db is True:
    
        logger.info('Open DB connection')
        connection = psycopg2.connect(
            dbname=os.getenv('MONITORING_DB_NAME'),
            user=os.getenv('MONITORING_DB_USER'),
            password=os.getenv('MONITORING_DB_PASSWORD'),
            host=os.getenv('MONITORING_DB_HOST'),
            port=int(os.getenv('MONITORING_DB_PORT'))
        )
        cursor = connection.cursor()
        cursor.execute(
            f'''
                CREATE TABLE IF NOT EXISTS tutorial_predict_model_metrics(
                    timestamp BIGINT,
                    key TEXT,
                    value REAL
                );
            '''
        )

        logger.info('Log monitoring metrics')

        for metric, values in metrics_df.set_index('month').to_dict().items():
            logger.debug(f'{metric}: {values}')

            # Log metrics in model monitoring metrics table to visualize in Grafana
            for step, value in values.items():
                timestamp = int(pd.to_datetime(step).timestamp() * 1000)
                cursor.execute(
                    '''
                        INSERT INTO tutorial_predict_model_metrics(
                            timestamp, key, value
                        ) VALUES(%s, %s, %s)
                    ''',
                    (timestamp, metric, value)
                )
        connection.commit()
        connection.close()


if __name__ == '__main__':

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args_parser.add_argument('--evaluation-date', dest='evaluation_date', required=False, default=None)
    args_parser.add_argument('--storage-path', dest='storage_path', default=None)
    args_parser.add_argument('--db', dest='db', action='store_true', default=False)

    args = args_parser.parse_args()

    model_monitoring(
        config_path=args.config,
        evaluation_date=args.evaluation_date,
        storage_path=args.storage_path,
        db=args.db
    )
