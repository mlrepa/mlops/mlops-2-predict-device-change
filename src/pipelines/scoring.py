"""This script scores data on trained model."""

import argparse
from datetime import date
from dateutil.relativedelta import relativedelta
import joblib
import pandas as pd
from pathlib import Path
import pickle
from typing import Text

from src.data.process import process_features
from src.utils.config import load_config
from src.utils.logging import get_logger
from src.utils.datetime_utils import check_isoformat


def scoring(
        config_path: Text,
        scoring_date: Text,
        storage_path: Text
) -> None:

    """Scores data on trained model.
    Args:
        config_path {Text}: path to config file
        scoring_date {Text}: date on which scoring is executed
        storage_path {Text}: path to data storage
    """

    config = load_config(config_path)
    logger = get_logger("SCORING", config.base.log_level)

    check_isoformat(scoring_date)
    print(f"Scoring date: {scoring_date}")

    logger.info(f"Scoring date: {scoring_date}")

    logger.info('Load model & encoders')
    model = joblib.load(config.train.model_path)

    with open(config.train.encoder_path, 'rb') as ef:
        encoder = pickle.load(ef)

    logger.info('Load & process dataset')

    if not storage_path:
        logger.warning(f'Storage path is not set. Default value will be used')
        storage_path = './data'

    storage_path = Path(storage_path)
    logger.info(f'Storage path = {storage_path}')

    scoring_data_dir = storage_path / config.data.scoring.dir_data_path
    scoring_data = pd.read_feather(scoring_data_dir / f'{scoring_date}.feather')
    scoring_data = process_features(scoring_data)

    logger.info('Prepare features')
    categories = config.featurize.categories
    scoring_data[categories] = encoder.transform(scoring_data[categories])
    f_matrix = scoring_data.drop(columns=['user_id', 'month'], axis=1).to_numpy()

    logger.info('Scoring')
    preds = model.predict(f_matrix)
    probas = model.predict_proba(f_matrix)
    logger.info(f'Max probas: {probas[:, 1].max()}')

    logger.info('Save scores')
    scoring_results_dir = storage_path / config.data.scoring.dir_results_path
    result_date = date.fromisoformat(scoring_date) + relativedelta(months=1, day=1)
    result_date = result_date.strftime('%Y-%m-%d')
    scoring_result_path = scoring_results_dir / f'{result_date}.feather'

    scores_df = scoring_data.loc[:, ['user_id']].copy()
    scores_df['scoring_date'] = scoring_date
    scores_df['label'] = preds.astype('int8')
    scores_df['score'] = probas[:, 1]
    scores_df.reset_index(drop=True).to_feather(scoring_result_path)
    logger.info(f'Scores saved to: {scoring_result_path}')

    return scores_df


if __name__ == '__main__':

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args_parser.add_argument('--scoring-date', dest='scoring_date', required=False, default=None)
    args_parser.add_argument('--storage-path', dest='storage_path', default=None)
    args = args_parser.parse_args()

    scoring(
        config_path=args.config,
        scoring_date=args.scoring_date,
        storage_path=args.storage_path
    )
