import argparse
import pandas as pd
from pathlib import Path
from typing import Text

from src.utils.config import load_config
from src.utils.logging import get_logger


def load_data(config_path: Text, storage_path: Text) -> None:
    """Load and process data.
    Args:
        config_path {Text}: path to config file
    """

    config = load_config(config_path)
    logger = get_logger("LOAD_DATA", config.base.log_level)

    if not storage_path:
        logger.warning(f'Storage path is not set. Default value will be used')
        storage_path = './data'

    dir_data_raw = Path(config.data.raw.dir_path)
    storage_path = Path(storage_path)
    logger.info(f'Storage path = {storage_path}')
    dir_data_storage = storage_path / config.data.raw.storage_dir_path

    logger.info('Load data')
    user_features = pd.read_feather(dir_data_storage / config.data.raw.user_features)
    target = pd.read_feather(dir_data_storage / config.data.raw.target)
    
    logger.info('Sample data')
    user_features = user_features.sample(
        frac=config.data_load.sample_size,
        random_state=config.base.random_state
    ).reset_index(drop=True)
    target = target[target['user_id'].isin(user_features['user_id'])].reset_index(drop=True)

    logger.info('Save data')
    user_features_path = dir_data_raw / config.data.raw.user_features
    user_features.to_feather(user_features_path)
    target_path = dir_data_raw / config.data.raw.target
    target.to_feather(target_path)
    logger.info('Save data complete')
    
    logger.info('Save data Index')
    user_features_index_path = dir_data_raw / config.data.raw.user_features_index
    user_features[['user_id']].to_csv(user_features_index_path, index=False)
    target_index_path = dir_data_raw / config.data.raw.target_index
    target[['user_id', 'target', 'month', 'date']].to_csv(target_index_path, index=False)
    logger.info('Save index complete')


if __name__ == '__main__':

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args_parser.add_argument('--storage-path', dest='storage_path', default=None)
    args = args_parser.parse_args()

    load_data(config_path=args.config, storage_path=args.storage_path)
