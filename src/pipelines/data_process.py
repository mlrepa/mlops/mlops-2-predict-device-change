import argparse
from pathlib import Path
from typing import Text

from src.data.load import load_target, load_data
from src.data.process import process_target, process_data
from src.utils.config import load_config
from src.utils.logging import get_logger


def data_process(config_path: Text) -> None:
    """Load and process data.
    Args:
        config_path {Text}: path to config file
    """

    config = load_config(config_path)
    logger = get_logger("DATA_LOAD", config.base.log_level)

    logger.info('Load dataset')
    dir_data_raw = Path(config.data.raw.dir_path)
    target_df = load_target(dir_data_raw / config.data.raw.target)
    data_df = load_data(dir_data_raw / config.data.raw.user_features)

    logger.info('Process target')
    target_df = process_target(target_df)

    logger.info('Process dataset')
    data_df = process_data(data_df)
    data_df.dropna(inplace=True)

    logger.info('Save processed data and target')
    processed_data_dir = Path(config.data.processed.dir_path)
    logger.debug(f'Processed data dir: {processed_data_dir}')
    target_df.to_feather(processed_data_dir / config.data.processed.target)
    data_df.to_feather(processed_data_dir / config.data.processed.user_features)


if __name__ == '__main__':

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()

    data_process(config_path=args.config)
