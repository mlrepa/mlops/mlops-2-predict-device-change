"""This script logs metadata to mlflow server."""

import argparse
import json
from pyexpat import features
import joblib
import mlflow
import mlflow.catboost
from mlflow.tracking import MlflowClient
import mlflow.sklearn
import os
import pandas as pd
from pathlib import Path
from typing import Text

from src.utils.config import load_config
from src.utils.errors import UnknownEstimatorError
from src.utils.logging import get_logger
from src.utils.mlflow_utils import create_mlflow_experiment


def log_to_mlflow(config_path: Text) -> None:
    """
    Logs metadata to mlflow server.
    Args:
        config_path {Text}: path to config
    """

    config = load_config(config_path)
    logger = get_logger('LOG_METRICS', config.base.log_level)
    
    logger.info('Start logging')
    if config.log_metrics.mlflow_run is True: 

        client = MlflowClient(tracking_uri=os.environ.get("MLFLOW_TRACKING_URI"),
                            registry_uri=os.environ.get("MLFLOW_STORAGE"))

        # Create new experiment & run
        exp_name = config.base.exp_name
        exp_id = create_mlflow_experiment(exp_name)
        run = client.create_run(experiment_id=exp_id, start_time=None, tags=None)
        logger.debug(f'Experiment ID: {exp_id}, Run ID: {run.info.run_id}')

        with mlflow.start_run(run_id=run.info.run_id, experiment_id=exp_id):

            logger.info('Log train metrics')
            with open(config.train.train_metrics) as tm_file:
                metrics = json.load(tm_file)
                mlflow.log_metrics(metrics)

            logger.info('Log cross-validation metrics (raw metrics, by folds')
            # (timestamp - int in miliseconds)
            
            raw_metrics_df = pd.read_csv(config.train.raw_metrics_path)
            for metric, values in raw_metrics_df.set_index('test_period').to_dict().items():
                logger.debug(f'{metric}: {values}')
                for step, value in values.items():
                    client.log_metric(
                        run_id=run.info.run_id,
                        key=metric,
                        value=value,
                        timestamp=int(pd.to_datetime(step).timestamp() * 1000))

            logger.info('Log params')

            estimator = config.train.estimator
            available_estimators = config.train.estimators
            estimator_params = available_estimators[estimator]

            mlflow.log_params({
                'random_state': config.base.random_state,
                'categories': json.dumps(config.featurize.categories),
                'estimator': estimator,
                'estimator_params': json.dumps(estimator_params),
                'top_K_coef': config.train.top_K_coef
            })

            logger.info('Log artifacts')
            processed_data_dir = Path(config.data.processed.dir_path)
            features_path = processed_data_dir / config.data.processed.features
            mlflow.log_artifact(config_path)
            mlflow.log_artifact(features_path)
            mlflow.log_artifact(config.train.lift_curve_image)
            mlflow.log_artifact(config.train.train_metrics_image)
            mlflow.log_artifact(config.train.encoder_path)

            logger.info('Log model')
            model = joblib.load(config.train.model_path)

            if estimator == 'catboost':
                mlflow.catboost.log_model(model, 'model')
            elif estimator == 'random_forest':
                mlflow.sklearn.log_model(model, 'model')
            else:
                raise UnknownEstimatorError(f'Unknown estimator: {estimator}')

            logger.info('Log mlflow report')

            with open(config.log_metrics.mlflow_report_path, 'w') as mrf:
                json.dump(
                    obj={
                        'tracking_uri': mlflow.get_tracking_uri(),
                        'exp_id': exp_id,
                        'exp_name': exp_name,
                        'run_id': run.info.run_id
                    },
                    fp=mrf
                )
            
    else:
        logger.warning('Logging metrics with MLflow - DISABLED (mlflow_run: False)')
        logger.info('Log empty mlflow report')
        with open(config.log_metrics.mlflow_report_path, 'w') as mrf:
            json.dump(
                obj={
                    'tracking_uri': '',
                    'exp_id': '',
                    'exp_name': '',
                    'run_id': ''
                },
                fp=mrf
            )
            


if __name__ == '__main__':

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()

    log_to_mlflow(config_path=args.config)
