from typing import Iterable, Text

import numpy as np
import pandas as pd
from pandas.plotting import table

from matplotlib.figure import Figure
from matplotlib import pyplot as plt


def pixels_in_inches(pixels: int = 1) -> float:
    """Converts pixels to inches bases on
    Args:
        pixels {int}: pixels number
    Returns:
        float: value in inches
    """
    return pixels / plt.rcParams['figure.dpi']


def dataframe_to_image(
        df: pd.DataFrame, filename: Text, decimals: int = 3,
        width: int = 600, height: int = 400, fontsize: float = 15
) -> None:
    """Plots train metrics.
    Args:
        df {pd.DataFrame}: dataframe
        filename {List}: image filename
        decimals {int}: number of decimal places to round data
        width {int}: image width in pixels
        height {int}: image height in pixels
        fontsize {float}: text font size
    """

    # Round data
    df = df.round(decimals)

    # Calc figure sizes
    px = pixels_in_inches()
    fig_width = width * px
    fig_height = height * px

    # Create subplot
    fig, ax = plt.subplots(figsize=(fig_width, fig_height))
    # Hide axes
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    # Hide frame
    ax.set_frame_on(False)
    # Build table
    tbl = table(
        ax,
        df,
        loc='center',
        cellLoc='center'
    )
    # Set font size
    tbl.auto_set_font_size(False)
    tbl.set_fontsize(fontsize)
    # Scale vertically to avoid rows overlapping
    tbl.scale(1, fig_height)

    # Save table to image
    fig.savefig(filename, transparent=True)


def build_lift_curve(
        y_true: Iterable, y_pred: Iterable, step: float = 0.01,
        width: int = 600, height: int = 400
) -> Figure:
    """Function that builds a Lift Curve Figure using the real label values of a dataset and
    the probability predictions of a Machine Learning Algorithm/model.
    Args:
        y_true {Iterable}: real labels of the data
        y_pred {Iterable}: probability predictions for such data
        step {float}: how big we want the steps in the percentiles to be
        width {int}: image width in pixels
        height {int}: image height in pixels
    Reference: https://towardsdatascience.com/the-lift-curve-unveiled-998851147871
    """

    # Define an auxiliar dataframe to plot the curve
    aux_lift = pd.DataFrame()
    # Create a real and predicted column for our new DataFrame and assign values
    aux_lift['real'] = y_true
    aux_lift['predicted'] = y_pred
    # Order the values for the predicted probability column:
    aux_lift.sort_values('predicted', ascending=False, inplace=True)

    # Create the values that will go into the X axis of our plot
    x_val = np.arange(step, 1 + step, step)
    # Calculate the ratio of ones in our data
    ratio_ones = aux_lift['real'].sum() / len(aux_lift)
    # Create an empty vector with the values that will go on the Y axis our our plot
    y_v = []

    # Calculate for each x value its correspondent y value
    for x in x_val:
        num_data = int(
            np.ceil(x * len(aux_lift)))  # The ceil function returns the closest integer bigger than our number
        data_here = aux_lift.iloc[:num_data, :]  # ie. np.ceil(1.4) = 2
        ratio_ones_here = data_here['real'].sum() / len(data_here)
        y_v.append(ratio_ones_here / ratio_ones)

    # Calc figure sizes
    px = pixels_in_inches()
    fig_width = width * px
    fig_height = height * px

    # Build the figure
    fig, axis = plt.subplots(figsize=(fig_width, fig_height))
    axis.plot(x_val, y_v, 'g-', linewidth=3, markersize=5)
    axis.plot(x_val, np.ones(len(x_val)), 'k-')
    axis.set_xlabel('Proportion of sample')
    axis.set_ylabel('Lift')
    axis.set_title('Lift Curve')

    return fig


def plot_lift_curve(
        y_true: Iterable, y_pred: Iterable, step: float = 0.01,
        width: int = 600, height: int = 400
) -> None:
    """
        Function that plots a Lift Curve using the real label values of a dataset and
    the probability predictions of a Machine Learning Algorithm/model.
    Args:
        y_true {Iterable}: real labels of the data
        y_pred {Iterable}: probability predictions for such data
        step {float}: how big we want the steps in the percentiles to be
        width {int}: image width in pixels
        height {int}: image height in pixels
    Reference: https://towardsdatascience.com/the-lift-curve-unveiled-998851147871
    """

    build_lift_curve(y_true, y_pred, step, width, height)
    plt.show()

