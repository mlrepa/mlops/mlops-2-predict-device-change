from datetime import date
from dateutil.relativedelta import relativedelta
import pandas as pd
from pandas.tseries.offsets import MonthEnd, MonthBegin
import re
from typing import Text


def first_month_day(dt: Text) -> Text:
    """Get date of the first month day.
    Args:
        dt {Text}: date in ISO format (YYYY-MM-DD)
    Returns:
        Text: date in ISO format
    """

    d = date.fromisoformat(dt)
    last_day = d + relativedelta(day=1)

    return last_day.strftime('%Y-%m-%d')


def last_month_day(dt: Text) -> Text:
    """Get date of the lat month day.
    Args:
        dt {Text}: date in ISO format (YYYY-MM-DD)
    Returns:
        Text: date in ISO format
    """

    d = date.fromisoformat(dt)
    last_day = d + relativedelta(day=31)
    return last_day.strftime('%Y-%m-%d')


def prev_day(dt: Text) -> Text:
    """Get date of previous day.
    Args:
        dt {Text}: date in ISO format (YYYY-MM-DD)
    Returns:
        Text: date in ISO format
    """
    return (date.fromisoformat(dt) + relativedelta(days=-1)).strftime('%Y-%m-%d')


def check_isoformat(date: Text) -> None:
    """Checks date type and format.
    Args:
        date {Text}: date
    Raises:
        TypeError: if date type is not string
        ValueError: if date string is not in ISO format (YYYY-MM-DD)
    """

    if not isinstance(date, str):
        raise TypeError(f'Invalid date type: {type(date)}, must be string')

    if not re.match(r'^\d{4}-\d\d-\d\d', date):
        raise ValueError('Bad date format. ISO format required: YYYY-MM-DD')


def get_scoring_date(dt: Text) -> Text:
    """Get first day of the scoring period (month)
    Args:
        dt {Text}: date in ISO format (YYYY-MM-DD)
    Returns:
        Text: date in ISO format
    """

    tr_month_start = pd.to_datetime(dt) + MonthBegin(1)
    return first_month_day(tr_month_start.strftime('%Y-%m-%d'))


def get_train_period_end(scoring_date: Text, delay: int = 1) -> Text:
    """Get last date of the available training data
    Args:
        dt {Text}: date in ISO format (YYYY-MM-DD)
        delay: time period (in months) between last train data point available and scoring period start
    Returns:
        Text: date in ISO format
    """

    tr_month_start = pd.to_datetime(scoring_date) - MonthEnd(delay+1)
    return last_month_day(tr_month_start.strftime('%Y-%m-%d'))


def get_evaluate_period_date(dt: Text, delay: int = 1) -> Text:
    """Get date of the lat month day.
    Args:
        dt {Text}: date in ISO format (YYYY-MM-DD)
    Returns:
        Text: date in ISO format
    """

    tr_month_start = pd.to_datetime(dt) - MonthEnd(delay)
    return first_month_day(tr_month_start.strftime('%Y-%m-%d'))

