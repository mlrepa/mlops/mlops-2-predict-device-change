import mlflow
from pathlib import Path
from typing import Text


def create_mlflow_experiment(experiment_name: Text, mode: oct = 0o777) -> Text:
    """
    Set mlflow experiment and permissions for folder
    Args:
        experiment_name {Text}: experiment name
        mode {oct}: experiment name mode, default = 0o777
    """

    mlflow.set_experiment(experiment_name)
    experiment = mlflow.get_experiment_by_name(experiment_name)
    artifact_location = Path(experiment.artifact_location)

    if not artifact_location.exists():
        artifact_location.mkdir(exist_ok=True, parents=True)
        artifact_location.chmod(mode)

    return experiment.experiment_id
